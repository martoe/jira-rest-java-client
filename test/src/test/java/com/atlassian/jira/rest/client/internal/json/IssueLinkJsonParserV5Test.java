package com.atlassian.jira.rest.client.internal.json;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import com.atlassian.jira.rest.client.TestUtil;
import com.atlassian.jira.rest.client.api.domain.IssueLink;
import com.atlassian.jira.rest.client.api.domain.IssueLinkType;
import org.codehaus.jettison.json.JSONException;
import org.junit.Test;

public class IssueLinkJsonParserV5Test {

  @Test
  public void parse() throws JSONException {
    IssueLinkJsonParserV5 parser = new IssueLinkJsonParserV5();
    final IssueLink issueLink = parser.parse(ResourceUtil.getJsonObjectFromResource("/json/issueLink/validV5.json"));
    assertThat(issueLink.getId(), equalTo("10001"));
    assertThat(issueLink.getTargetIssueKey(), equalTo("PRJ-2"));
    assertThat(issueLink.getTargetIssueUri(), equalTo(TestUtil.toUri("http://www.example.com/jira/rest/api/2/issue/PRJ-2")));
    assertThat(issueLink.getIssueLinkType(), equalTo(new IssueLinkType("Dependent", "is depended by", IssueLinkType.Direction.OUTBOUND)));
  }
}
