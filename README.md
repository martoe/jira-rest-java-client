## Links

* [REST API](https://docs.atlassian.com/jira/REST/5.2.11/)
* [Issues](https://ecosystem.atlassian.net/projects/JRJC)


## Building

* Use Maven 2 (see [JRJC-176](https://ecosystem.atlassian.net/browse/JRJC-176))

        set M2_HOME=c:\dev\apache-maven-2.2.1
        set PATH=%M2_HOME%\bin;%PATH%

* Install the JTA library manually (see [JRJC-77](https://ecosystem.atlassian.net/browse/JRJC-77))

        mvn install:install-file -DgroupId=jta -DartifactId=jta -Dversion=1.0.1 -Dpackaging=jar -Dfile=jta-1_0_1B-classes.zip
        mvn deploy:deploy-file   -DgroupId=jta -DartifactId=jta -Dversion=1.0.1 -Dpackaging=jar -Dfile=jta-1_0_1B-classes.zip -DrepositoryId=nexus -Durl=http://nexus.bawagpsk.com/content/repositories/thirdparty 

* Add a profile to ~/.m2/settings.xml:

        <profile>
          <id>atlassian</id>
          <activation>
            <activeByDefault>true</activeByDefault>
          </activation>
          <repositories>
            <repository>
              <id>atlassian-public</id>
              <url>https://maven.atlassian.com/repository/public</url>
            </repository>
          </repositories>
          <pluginRepositories>
            <pluginRepository>
              <id>atlassian-public</id>
              <url>https://maven.atlassian.com/repository/public</url>
            </pluginRepository>
          </pluginRepositories>
        </profile>

* Build the artifacts

        mvn clean install -DskipITs
        mvn deploy -pl core -am -DskipITs -DaltDeploymentRepository=nexus::default::http://nexus.bawagpsk.com/content/repositories/releases
